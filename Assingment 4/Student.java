package modelos;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;


public class Student {

    private int    id_Student;
    private String name;
    private String name1;
    private String name2;
    private String DOB;

    public int getId_Student() {
        return id_Student;
    }

    public String getName() {
        return name;
    }

    public String getName1() {
        return name1;
    }

    public String getName2() {
        return name2;
    }

    public String getDOB() {
        return DOB;
    }


    public Student (int id_Student,String name, String name1, String name2, String DOB){

        this.id_Student = id_Student;
        this.name       = name;
        this.name1      = name1;
        this.name2      = name2;


        char[] StringtoChar = DOB.toCharArray();
        /*Arrays.sort(StringtoChar);
        String SortedString = new String(StringtoChar);*/

        String nuevo = String.valueOf(StringtoChar[6])+ String.valueOf(StringtoChar[7]) + String.valueOf(StringtoChar[8]) + String.valueOf(StringtoChar[9])
                + String.valueOf(StringtoChar[5]) + String.valueOf(StringtoChar[3])+String.valueOf(StringtoChar[4])
                + String.valueOf(StringtoChar[2]) + String.valueOf(StringtoChar[0]) + String.valueOf(StringtoChar[1]);


        this.DOB        = nuevo;


    }

}
