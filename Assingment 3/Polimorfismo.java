
    class Food {
        public void foodIngred() {
            System.out.println("The food have some Ingredients");
        }
    }

    class Hamburger extends Food {
        public void foodIngred() {
            System.out.println("The Hamburger have: bacon");
        }
    }

    class Pizza extends Food {
        public void foodIngred() {
            System.out.println("The pizza have: Peperoni");
        }
    }

    class Main2 {
        public static void main(String[] args) {
            Food myFood      = new Food();
            Food myHamburger = new Hamburger();
            Food myPizza     = new Pizza();

            myFood.foodIngred();
            myHamburger.foodIngred();
            myPizza.foodIngred();
        }
    }

